<?php
use App\Person;
use App\Student;

//require_once("../../src/BITM/SEIP136344/Person.php");
//require_once("../../src/BITM/SEIP136344/Student.php");

function __autoload($className){
    echo $className."<br>"; // App\Person
    list($ns,$cn)=explode("\\",$className);
    require_once("../../src/BITM/SEIP136344/".$cn.".php");
}

$obj=new Person();
echo $obj->showPersonInfo();

$obj=new Student();
echo $obj->showStudentInfo();
?>