<?php
namespace App;

class Person{
    public $name="Charles";
    public $gender="Male";
    public $blood_group="B+";

    public function showPersonInfo(){
        echo $this->name."<br>";
        echo $this->gender."<br>";
        echo $this->blood_group."<br>";
    }
}
?>

